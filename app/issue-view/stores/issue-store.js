var Flux = require('sync-flux');
var Api = require('../../api/api');

var Store = Flux.createStore(function(){
    this.issue = null;

    this.loadIssue = function(params){
        Api('/repos/' + params.username + '/' + params.repository + '/issues/' + params.repository_id, 'GET').then(function(response){
            this.issue = response;
            this.emit();
        }.bind(this))
    };

    this.listen({
        'issue_view:loadIssue': this.loadIssue
    })
});

module.exports = Store;