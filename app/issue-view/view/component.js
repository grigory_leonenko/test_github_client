var React = require('react');
var Flux = require('sync-flux');
var StatusBar = require('../../status-bar/view/component');
var IssueStore = require('../stores/issue-store');
var DateFormat = require('dateformat');

var Component = React.createClass({
    mixins: [IssueStore.syncMixin(['issue'])],
    componentDidMount: function(){
        Flux.dispatch('issue_view:loadIssue')(this.props);
    },
    render: function(){
        return  (<span>
                    <StatusBar back = {true} />
                    <div className = "issue-view-page">
                        {this.state.issue ? <Issue issue = {this.state.issue} /> : null}
                    </div>
                </span>)
    }
});

var Issue = React.createClass({
    render: function(){
        return (<span>
                    <div className = "head">
                    <User user = {this.props.issue.user}/>
                        <Info created = {this.props.issue.created_at} state = {this.props.issue.state} title = {this.props.issue.title} />
                    </div>
                    <div className = "body">{this.props.issue.body}</div>
                </span>)
    }
})

var User = React.createClass({
    render: function(){
        return (<div className = "issue-user">
                    <img className = "avatar" src={this.props.user.avatar_url} />
                    <div className = "login">{this.props.user.login}</div>
                </div>)
    }
});

var Info = React.createClass({
    render: function(){
        return (<div className = "issue-info">
                    <div className="info-title">{this.props.title}</div>
                    <div className="info-created">{DateFormat(this.props.created, "fullDate")}</div>
                    <div className={'info-state ' + this.props.state}>{this.props.state}</div>
                </div>)
    }
});


module.exports = Component;