var Flux = require('sync-flux');

var Store = Flux.createStore(function(){
    this.status = {
        type: null,
        message: null
    };

    this.errorApply = function(error){
        this.status = {
            type: 'error',
            message: error
        };
        this.emit();
    };

    this.messageApply = function(message){
        this.status = {
            type: 'status',
            message: message
        };
        this.emit();
    };

    this.clearStatus = function(){
        this.status = {
            type: null,
            message: null
        };
        this.emit();
    };

    this.listen({
        'issues_status:error': this.errorApply,
        'issues_status:message': this.messageApply,
        'issues_status:clear': this.clearStatus,
        'issues_all:clear': this.clearStatus
    })
});

module.exports = Store;