var Flux = require('sync-flux');
var Api = require('../../api/api');

var Store = Flux.createStore(function(){
    var $secret_user = null;
    this.user = null;
    this.repo_name = null;
    this.user_repos = null;

    this.searchUser = function(){
        if((this.user === $secret_user) || !this.user){
            return;
        } else {

            this.dispatch('issues_all:clear')();

            this.dispatch('issues_status:message')(message_txt('start', this.user));

            $secret_user = this.user;

            Api('/users/' + this.user, 'GET').then(function(user){
                this.searchRepos(this.user);
            }.bind(this), function(error){
                this.dispatch('issues_status:error')(message_txt('nouser', this.user));
            }.bind(this));
        };
    };

    this.setUser = function(name){
        this.user = name;
        this.emit();
    };

    this.setRepoName = function(name){
        this.repo_name = name;
        this.emit();
    };

    this.searchRepos = function(name){
        Api('/users/' + name + '/repos', 'GET').then(function(repos){
            this.user_repos = repos;
            this.dispatch('issues_status:clear')();
            this.emit();
        }.bind(this), function(error){
            this.dispatch('issues_status:error')(message_txt('norepository', name));
        }.bind(this))
    };

    this.searchIssues =  function(repository){
        this.dispatch('issues_list:searchIssues')(this.user, repository);
    };

    this.clearStore = function(){
        this.repo_name = null;
        this.user_repos = null;
        this.emit();
    };

    function message_txt(type, name){
        var $messages = {
            'start': 'Searching ' + name +' and ' + name + ' repositories...',
            'nouser': 'Error ' + name +' searching!',
            'norepository': 'Error ' + name +' repositories searching!'
        };
        return $messages[type];
    };

    this.listen({
        'issues_user:searchUser': this.searchUser,
        'issues_user:searchIssues': this.searchIssues,
        'issues_user:setUser': this.setUser,
        'issues_user:setRepoName': this.setRepoName,
        'issues_all:clear': this.clearStore
    });

});

module.exports = Store;