var Flux = require('sync-flux');
var Api = require('../../api/api');

var Store = Flux.createStore(function(){
    this.issues = [];

    this.searchIssues = function(login, repository){

        this.dispatch('issues_status:message')(message_txt('start', repository));

        Api('/repos/' + login  + '/' + repository + '/issues', 'GET').then(function(issues){
            this.issues = issues;
            if(issues.length == 0){
                this.dispatch('issues_status:message')(message_txt('empty', repository));
            } else {
                this.dispatch('issues_status:clear')();
            };
            this.emit();
        }.bind(this), function(error){
            this.dispatch('issues_status:error')(message_txt('error', repository));
        }.bind(this));
    };

    function message_txt(type, repository){
        var $messages = {
            'start': 'Searching issues for repository ' + repository + ' ...',
            'empty': 'Sorry no issues for ' + repository,
            'error': 'Error loading issues for ' + repository
        };
        return $messages[type];
    };

    this.clearStore = function(){
        this.issues = [];
        this.emit();
    };

    this.listen({
        'issues_list:searchIssues': this.searchIssues,
        'issues_all:clear': this.clearStore
    });
});

module.exports = Store;