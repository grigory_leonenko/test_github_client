var React = require('react');
//Components
var SearchBlock = require('./search-block');
var List = require('./issues-list');
var StatusBlock = require('./status-block');
var StatusBar = require('../../status-bar/view/component');
//Stores
var IssuesStore = require('../stores/issues-store');
var UserStore = require('../stores/user-store');
var StatusStore = require('../stores/status-store');

var Component = React.createClass({
    mixins: [IssuesStore.syncMixin('issues'), UserStore.syncMixin('user_repos'), StatusStore.syncMixin('status')],
    render: function(){
        return (<div className="issues-list-page">
                    <StatusBar />
                    <SearchBlock user_repos = {this.state.user_repos} />
                    <StatusBlock status = {this.state.status} />
                    {this.state.issues.length > 0 ? <List issues = {this.state.issues} /> : null}
                </div>)
    }
});




module.exports = Component;