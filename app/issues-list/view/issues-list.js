var React = require('react');
var Link = require('react-router-component').Link;
var DateFormat = require('dateformat');

var List = React.createClass({
    makeIssues: function(ctx){
        return ctx.issues.map(function(issues){
            return <Issue issue = {issues} />
        })
    },
    render: function(){
        return (<div className = "issues-list">
                    <h4>Issues:</h4>
                    <ul className="list"><Heading />{this.makeIssues(this.props)}</ul>
                </div>)
    }
});

var Heading = React.createClass({
    render: function(){
        return (<li className="heading">
                    <span className="cell wh-1">Number</span>
                    <span className="cell wh-7">Title</span>
                    <span className="cell wh-2">Creation date</span>
                </li>)
    }
});

var Issue = React.createClass({
    getLink: function(issue){
        return issue.url.replace('https://api.github.com/repos/', '/issue/').replace('issues/', '')
    },
    render: function(){
        return (<li className="row">
                    <Link href={this.getLink(this.props.issue)}>
                        <span className = "cell wh-1">{this.props.issue.number}</span>
                        <span className = "cell wh-7">{this.props.issue.title}</span>
                        <span className = "cell wh-2">{DateFormat(this.props.issue.created_at, "fullDate")}</span>
                    </Link>
                </li>)
    }
});

module.exports = List;