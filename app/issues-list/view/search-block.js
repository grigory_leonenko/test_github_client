var React = require('react');
var Flux = require('sync-flux');
var UserStore = require('../stores/user-store');

var WelcomeSearch = React.createClass({
    render: function(){
        return (<div className="search-block">
                    <h3>Github issues viewer.</h3>
                    <div className="global clarification">First type user name and press enter for search repositories:</div>
                    <NameInput />
                    {this.props.user_repos ? <RepositoryInput repositories = {this.props.user_repos} /> : null}
                </div>)
    }
});


/**
 * Github user name input, search user after blur event
 * */

 var NameInput = React.createClass({
    mixins: [UserStore.syncMixin('user')],
    searchUser: function(){
        Flux.dispatch('issues_user:searchUser')();
    },
    onChange: function(event){
        var $name = event.target.value;
        Flux.dispatch('issues_user:setUser')($name);
        if(!$name) Flux.dispatch('issues_all:clear')();
    },
    onKeyDown: function(event){
        if(event.keyCode === 13) this.searchUser();
    },
    render: function(){
        return (<span className="input">
                    <input type="text" className={this.state.user ? "input-field filled" : "input-field"} value={this.state.user} onKeyDown = {this.onKeyDown} onChange = {this.onChange} onBlur = {this.searchUser} />
                    <span className="input-placeholder">User name</span>
                </span>)
    }
});


/**
 * Repository name input component with simple suggestion simple, available only after user been finded
 * */

var RepositoryInput = React.createClass({
    mixins: [UserStore.syncMixin('repo_name')],
    getInitialState: function(){
        return {suggestion: ''}
    },
    propTypes: {
        repositories: React.PropTypes.array
    },
    filterSuggestions: function(name){
        var $matched = '';

        this.props.repositories.map(function(repository){
            if($matched) return;
            if(repository.name.substr(0, name.length) === name && name) $matched = repository.name;
        });

        this.setState({suggestion: $matched});
        Flux.dispatch('issues_user:setRepoName')(name)
    },
    searchIssues: function(name){
        Flux.dispatch('issues_user:searchIssues')(name);
    },
    onChange: function(event){
        this.filterSuggestions(event.target.value);
    },
    onKeyDown: function(event){
        if(event.keyCode === 13){
            this.searchIssues(this.state.repo_name);
            this.setState({suggestion: []});
            event.preventDefault();
        };

        if(event.keyCode === 9 && this.state.suggestion){
            this.searchIssues(this.state.suggestion);
            Flux.dispatch('issues_user:setRepoName')(this.state.suggestion);
            this.setState({suggestion: []});
            event.preventDefault();
        };

    },
    render: function(){
        return (<span className="input">
                    <input type="text" value = {this.state.repo_name} className={this.state.repo_name ? "input-field filled" : "input-field"} onKeyDown = {this.onKeyDown}  onChange={this.onChange}/>
                    <span className="input-placeholder">Repository name</span>
                    <span className="input-suggestion">{this.state.suggestion}</span>
                </span>)
    }
})

module.exports = WelcomeSearch;