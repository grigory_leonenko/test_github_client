var React = require('react');

var Block = React.createClass({
    render: function(){
        return this.props.status.type ? <div className={'status-block ' + this.props.status.type}>{this.props.status.message}</div> : null;
    }
});

module.exports = Block;