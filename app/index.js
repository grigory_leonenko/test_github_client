var React = require('react');
var IssuesList = require('./issues-list/view/component');
var IssueView = require('./issue-view/view/component');
var Router = require('react-router-component');
var Location = Router.Location;
var Locations = Router.Locations;

var App = React.createClass({

    render: function() {
        return (
            <Locations>
                <Location path="/" handler={IssuesList} />
                <Location path="/issue/:username/:repository/:repository_id" handler={IssueView} />
            </Locations>
        )
    }
});


React.render(<App />, document.body);


