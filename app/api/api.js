var Promise = require('es6-promise').Promise;
var Flux = require('sync-flux');

var Api = (function(){
    return function(url, method, params){
        Flux.dispatch('status_bar:message')('Fetching data');
        var http = new XMLHttpRequest();
        var params = JSON.stringify(params);
        http.open(method, "/api" + url, true);
        http.setRequestHeader("Content-type", "application/json");
        http.setRequestHeader("Accept", "application/json");
        var promise = new Promise(function(resolve, reject){
            http.onreadystatechange = function() {//Call a function when the state changes.
                if(http.readyState == 4 && (http.status == 200 || http.status == 201)) {
                    var _data = JSON.parse(http.responseText);
                    Flux.dispatch('status_bar:clear')();
                    resolve(_data);
                } else if(http.readyState == 4 && http.status !== 200) {
                    Flux.dispatch('status_bar:error')('Api error ' + http.status + ' on request ' + url);
                    reject(http.status);
                }
            };
        })
        http.send(params);
        return promise;
    };
})();

module.exports = Api;