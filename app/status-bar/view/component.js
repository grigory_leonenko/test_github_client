var React = require('react');
var StatusBarStore = require('../stores/status-store');
var Link = require('react-router-component').Link;

var StatusBar = React.createClass({
    mixins: [StatusBarStore.syncMixin(['status'])],
    render: function(){
        return (<div className="status-bar">
                    {this.state.status ? <div className="bar-status">{this.state.status.message}</div> : null}
                    {this.props.back ? <Link href="/" className="back-btn">Go back to issues list &larr;</Link> : null}
                </div>)
    }
});

module.exports = StatusBar;