var Flux = require('sync-flux');

var Store = Flux.createStore(function(){
    this.status = null;

    this.errorApply = function(error){
        this.status = {
            type: 'error',
            message: error
        };
        setTimeout(this.clearStatus.bind(this), 3000);
        this.emit();
    };

    this.messageApply = function(message){
        this.status = {
            type: 'status',
            message: message
        };
        setTimeout(this.clearStatus.bind(this), 3000);
        this.emit();
    };

    this.clearStatus = function(){
        this.status = null;
        this.emit();
    };

    this.listen({
        'status_bar:error': this.errorApply,
        'status_bar:message': this.messageApply,
        'status_bar:clear': this.clearStatus
    })
});

module.exports = Store;