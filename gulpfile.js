var gulp = require('gulp'),
    gulp_watch = require('gulp-watch'),
    gulp_clean = require('gulp-clean'),
    gulp_sass = require('gulp-sass'),
    gulp_autoprefixer = require('gulp-autoprefixer'),
    gulp_uglify = require('gulp-uglify'),
    gulp_plumber  = require('gulp-plumber'),
    gulp_debug = require('gulp-debug'),
    connect = require('connect'),
    connect_fallback = require('connect-history-api-fallback'),
    serve_static = require('serve-static'),
    run_sequence = require('run-sequence'),
    browserify = require('browserify'),
    vinyl_source_stream = require('vinyl-source-stream'),
    reactify = require('reactify'),
    proxy = require('proxy-middleware'),
    url = require('url');

var tmp_fld = 'tmp';
var dist_fld = 'dist';

gulp.task('server', function(){
    return connect()
        .use('/api', proxy(url.parse('https://api.github.com/')))
        .use(connect_fallback)
        .use(serve_static(tmp_fld))
        .use(serve_static('bower_components'))
        .listen(9000);
});

gulp.task('watch', function(){
    gulp_watch('app/**/*.js', function(files){
        gulp.start('browserify');
    });
    gulp_watch('app/**/*.scss', function(files){
        gulp.start('sass');
    });
});

gulp.task('browserify', function(){
    return browserify('./app/index.js', {debug: true}).transform(reactify)
        .bundle()
        .pipe(vinyl_source_stream('app.js'))
        .pipe(gulp.dest('./' + tmp_fld + '/'))
});

gulp.task('sass', function(){
    return gulp.src('app/index.scss')
        .pipe(gulp_plumber())
        .pipe(gulp_sass({errLogToConsole: true}))
        .pipe(gulp_autoprefixer())
        .pipe(gulp.dest('./' + tmp_fld + '/'));
});

gulp.task('clone', function(){
    return gulp.src(['app/**/*.html', 'app/**/*.css', 'app/**/*.png', 'app/**/*.gif'], {base: 'app/'})
        .pipe(gulp_debug())
        .pipe(gulp.dest('./' + tmp_fld + '/'));
});

gulp.task('clean', function(){
    return gulp.src([tmp_fld + '/*'], {read: false})
        .pipe(gulp_clean());
});


gulp.task('default', function(){
    run_sequence('clean', 'browserify', 'sass', 'clone', ['watch', 'server']);
});